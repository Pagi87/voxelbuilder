﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ParrticlesInGrid : MonoBehaviour
{

    ParticleSystem system;
    Dictionary<Int3, ParticleSystem.Particle> particles = new Dictionary<Int3, ParticleSystem.Particle>();
    bool dirty = false;

    void Start()
    {
        system = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        if (dirty)
        {
            dirty = false;
            system.SetParticles(particles.Values.ToArray(), particles.Count);
        }
    }

    public void AddParticle(Int3 pos, ParticleSystem.Particle particle)
    {
        particles[pos] = particle;
        dirty = true;
    }
    public void RemoveParticle(Int3 pos)
    {
        particles.Remove(pos);
        dirty = true;
    }
}
