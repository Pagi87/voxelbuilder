﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorPanel : MonoBehaviour {
    public static Color CurrentColor = Color.white;
    public Image image;
    public void SetR(float f) {
        CurrentColor.r = f;
        image.color = CurrentColor;
    }
    public void SetG(float f)
    {
        CurrentColor.g = f;
        image.color = CurrentColor;
    }
    public void SetB(float f)
    {
        CurrentColor.b = f;
        image.color = CurrentColor;
    }
    public void SetA(float f)
    {
        CurrentColor.a = f;
        image.color = CurrentColor;
    }
}
