﻿using UnityEngine;
using System.Collections;
using System;

[System.Serializable]
public struct Int3 : System.IEquatable<Int3>
{
    public int x, y, z;

    public static readonly Int3 up = new Int3(0, 1);
    public static readonly Int3 down = new Int3(0, -1);
    public static readonly Int3 left = new Int3(-1, 0);
    public static readonly Int3 right = new Int3(1, 0);
    public static readonly Int3 zero = new Int3(0, 0, 0);
    public static readonly Int3 one = new Int3(1, 1, 1);
    public static readonly Int3 forward = new Int3(0, 0, 1);
    public static readonly Int3 backward = new Int3(0, 0, -1);


    public Int3(int X, int Y, int Z)
    {
        x = X;
        y = Y;
        z = Z;
    }
    public Int3(int X, int Y)
    {
        x = X;
        y = Y;
        z = 0;
    }
    public override string ToString()
    {

        return "x: " + x + " y: " + y + " z: " + z;
    }
    public override int GetHashCode()
    {
        unchecked
        {
            int hash = 7;

            hash += x + y * 691 + z * 2393;

            return hash;
        }
    }

    public override bool Equals(object obj)
    {
        return GetHashCode() == obj.GetHashCode();
    }

    public static float Distance(Int3 a, Int3 b)
    {
        var x = a.x - b.x;
        var y = a.y - b.y;
        var z = a.z - b.z;
        return Mathf.Sqrt(x * x + y * y + z * z);
    }

    public static Int3 Floor(Vector3 v)
    {
        return new Int3(Mathf.FloorToInt(v.x), Mathf.FloorToInt(v.y), Mathf.FloorToInt(v.z));
    }

    bool IEquatable<Int3>.Equals(Int3 other)
    {
        return (x == other.x && y == other.y && z == other.z);
    }

    public static explicit operator Vector2(Int3 i)
    {
        return new Vector2(i.x, i.y);
    }

    public static implicit operator Int3(Vector3 v)
    {
        return new Int3(Mathf.RoundToInt(v.x), Mathf.RoundToInt(v.y), Mathf.RoundToInt(v.z));
    }
    public static implicit operator Vector3(Int3 i)
    {
        return new Vector3(i.x, i.y, i.z);
    }

    public static bool operator ==(Int3 a, Int3 b)
    {
        return (a.x == b.x && a.y == b.y && a.z == b.z);
    }
    public static bool operator !=(Int3 a, Int3 b)
    {
        return !(a.x == b.x && a.y == b.y && a.z == b.z);
    }
    public static Int3 operator +(Int3 a, Int3 b)
    {
        a.x += b.x;
        a.y += b.y;
        a.z += b.z;
        return a;
    }
    public static Int3 operator *(Int3 a, Int3 b)
    {
        a.x *= b.x;
        a.y *= b.y;
        a.z *= b.z;
        return a;
    }
    public static Int3 operator -(Int3 a, Int3 b)
    {
        a.x -= b.x;
        a.y -= b.y;
        a.z -= b.z;
        return a;
    }
    public static Int3 operator -(Int3 a)
    {
        return new Int3(-a.x, -a.y, -a.z);
    }
    public static Int3 operator *(Int3 a, int b)
    {
        return new Int3(a.x * b, a.y * b, a.z * b);
    }
    public static Int3 operator *(int b, Int3 a)
    {
        return new Int3(a.x * b, a.y * b, a.z * b);
    }
    public static Int3 operator /(Int3 a, int b)
    {
        return new Int3(a.x / b, a.y / b, a.z / b);
    }
    public Int3 normalized
    {
        get
        {
            return new Int3(x == 0 ? 0 : x / Mathf.Abs(x), y == 0 ? 0 : y / Mathf.Abs(y), z == 0 ? 0 : z / Mathf.Abs(z));
        }
    }
}